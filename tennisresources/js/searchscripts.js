
jQuery(document).ready(function () {

    var currenturl = window.location.toString();
    var windowwidth = jQuery(window).width();
    var lastChar = currenturl.slice(-1);
    var pageurl = currenturl.replace(/%20| /g, '-');
    var bodyclass = jQuery(document.body).attr('class');
    var queryvals = jQuery.parseParams(currenturl.split('?')[1] || '');
    var filtvalue = "";
    var searchstr = "";
    var afterstring = "";
    var beforestring = "";
    var strstart = "";
    var strend = "";


// SEARCH FIELD HOME PAGE
    jQuery("a#homesearchbtn").click(function() {
        var searchterm = jQuery(".homesearch").val();
        var searchpage = "/video/?_sf_s=" + searchterm;
        window.location.href = searchpage;
    });
//END SEARCH FIELD HOME PAGE 

    if (bodyclass.indexOf('search') != -1) {
        jQuery("h2.custom-page-title").html('Search Results');
    }

    //PERSIST LIST VIEW IF CHANGING FILTERS
    //alert(document.cookie);

    if (document.cookie.indexOf('viewselect') != -1) {
        var viewcookie = Cookies.get('viewselect');
        if (viewcookie == 'list') {
            jQuery("section#main").addClass('listvid');
            jQuery("section#main").removeClass('gridvid');
            jQuery(".woo-image").removeClass('aligncenter');
            jQuery(".woo-image").addClass('alignleft');
            jQuery("a.listbtn").addClass('active');
        }
        else if (viewcookie == 'grid') {
            jQuery("section#main").addClass('gridvid');
            jQuery("section#main").removeClass('listvid');
            jQuery(".woo-image").addClass('aligncenter');
            jQuery(".woo-image").removeClass('alignleft');
            jQuery("a.gridbtn").addClass('active');
        }
    }
    else {
        jQuery("section#main").addClass('gridvid');
        jQuery("section#main").removeClass('listvid');
        jQuery(".woo-image").addClass('aligncenter');
        jQuery(".woo-image").removeClass('alignleft');
        jQuery("a.gridbtn").addClass('active');   
    }

    //END PERSIST LIST VIEW

    
    
// ADD FILTER LINKS ABOVE VIDEOS
    if (bodyclass.indexOf('video') != -1) {

        if (currenturl.indexOf('_sf') != -1 && bodyclass.indexOf('single') == -1) {

            jQuery.each(queryvals, function (index, item) {
                if (index.indexOf('_sft') != -1 || index.indexOf('_sfm') != -1) {

                    var itemvals = item.split(/ /g);

                    jQuery.each(itemvals, function (index, item) {
                        jQuery(".filtlinks").append("<a href='#' class='" + item + "'>" + item + " <i class='fa fa-times'></i></a>");
                    });

                    jQuery("h2.custom-page-title").html(item);

                }
                if (index.indexOf('_sf_s') != -1) {

                    var itemclass = item.replace(/ /g, '-');
                    jQuery(".filtlinks").append("<a href='#' class='" + itemclass + "'>" + item + " <i class='fa fa-times'></i></a>");

                    jQuery("h2.custom-page-title").html(item);

                }

            });

//END ADD FILTER LINKS ABOVE VIDEOS


//FILTER LINK CLICK EVENTS - REMOVE FILTERS FROM URL
            jQuery(".filtlinks a").click(function () {
                var linkclass = jQuery(this).attr('class');

                jQuery.each(queryvals, function (index, item) {
                    if (index.indexOf('_sf_s') != -1) {
                        item = item.replace(/ /g, '-');
                    }
                    //alert("item: " + item + ", linkclass: " + linkclass);
                    if (item.indexOf(linkclass) != -1) {
                        //alert("True! " + linkclass + " does occur within " + item + ".");
                        
                        if (index.indexOf('_sft') != -1) {
                            
                            if (item.indexOf(" ") != -1) {
                                
                                var itemvals = item.split(/ /g);
                                
                                jQuery.each(itemvals, function (itemkey, itemvalue) {
                                    if (itemvalue == linkclass) {
                                        //alert(itemvalue + " is a match!");
                                        afterstring = currenturl.charAt(pageurl.indexOf(itemvalue) + (itemvalue.length));
                                        beforestring = currenturl.charAt(pageurl.indexOf(itemvalue) - 1);
                                        //alert(afterstring);
                                        if (afterstring == "+") {
                                            searchstr = itemvalue + "+";
                                        }
                                        if (beforestring == "+" && afterstring == '') {
                                            searchstr = "+" + itemvalue;
                                        }
                                        if (beforestring == "+" && afterstring == '&') {
                                            searchstr = "+" + itemvalue;
                                        }
                                    }
                                    else {
                                        //alert("not a match!");
                                    }
                                });

                            }
                            else {
                                //alert("nope. no spaces;");
                                searchstr = index + "=" + item;
                            }

                        }
                        else if (index.indexOf('_sf_s') != -1) {
                            //item = item.replace(/ /g, '-');
                            searchstr = index + "=" + item;
                        }
                        else {
                            searchstr = index + "=" + item;
                        }

                        strstart = pageurl.indexOf(searchstr);
                        strend = pageurl.indexOf(searchstr) + searchstr.length;

                        //alert('Searching for: ' + searchstr + '. It begins at ' + strstart + ' and ends at ' + strend + '.');


                        var urlsec1 = pageurl.slice(0, strstart);
                        var urlsec2 = pageurl.slice(strend);

                        var newurl = urlsec1 + urlsec2;
                        //alert(newurl);

                        if (newurl.indexOf("&&") != -1) {
                            newurl = newurl.replace("&&", "&");
                        }

                        if (newurl.indexOf("?&") != -1) {
                            newurl = newurl.replace("?&", "?");
                        }
                        if (newurl.slice(-1) == "&") {
                            newurl = newurl.slice(0, -1);
                        }
                        if (newurl.slice(-1) == "?") {
                            newurl = newurl.slice(0, -1);
                        }
                        if (newurl.indexOf("&&") == -1 && newurl.indexOf("?&") == -1 && newurl.slice(-1) != "&" && newurl.slice(-1) != "?") {

                        }

                        window.location.href = newurl;
                    }
                    else {
                        //alert("False! " + linkclass + " does not occur within " + item + ".");
                        
                    }

                });
            });
        }
        else if (currenturl.indexOf('_sf') == -1 && bodyclass.indexOf('single') == -1) {
            jQuery("h2.custom-page-title").html('TENNIS RESOURCES MEDIA');
        }
        else {
        }

// END FILTER LINK CLICK EVENTS

        // BEGIN LIST vs. GRID VIEW SELECTION
            

        jQuery(".viewselector a").hover(function () {
            jQuery(this).css('cursor', 'pointer');
        });
        jQuery("a.gridbtn").click(function () {
            Cookies.remove('viewselect');
            Cookies.set('viewselect', 'grid');
            jQuery("section#main").addClass('gridvid');
            jQuery("section#main").removeClass('listvid');
            jQuery(".woo-image").addClass('aligncenter');
            jQuery(".woo-image").removeClass('alignleft');
            jQuery("a.gridbtn").addClass('active');
            jQuery("a.listbtn").removeClass('active');
            
        });
        jQuery("a.listbtn").click(function () {
            Cookies.remove('viewselect');
            Cookies.set('viewselect', 'list');
            jQuery("section#main").addClass('listvid');
            jQuery("section#main").removeClass('gridvid');
            jQuery(".woo-image").removeClass('aligncenter');
            jQuery(".woo-image").addClass('alignleft');
            jQuery("a.listbtn").addClass('active');
            jQuery("a.gridbtn").removeClass('active');

        });
// END LIST vs. GRID VIEW SELECTION
    



// BEGIN TOP RIGHT "SORT BY DROPDOWN
    jQuery("#mediaselect").change(function () {
        //var mediatype = jQuery("#mediaselect").val();
        var mediasort = jQuery("#mediaselect").val();

        /*if (mediatype == 'video') {

            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?_sfm_media_type_id=Video';
                window.location.href = newpage;
            }
            else {
                var newpage = currenturl + '&_sfm_media_type_id=Video';
                window.location.href = newpage;
            }

        }
        else if (mediatype == 'audio') {
            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?_sfm_media_type_id=Audio';
                window.location.href = newpage;
            }
            else {
                var newpage = currenturl + '&_sfm_media_type_id=Audio';
                window.location.href = newpage;
            }
        }
        else if (mediatype == 'article') {
            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?_sfm_media_type_id=Article';
                window.location.href = newpage;
            }
            else {
                var newpage = currenturl + '&_sfm_media_type_id=Article';
                window.location.href = newpage;
            }
        }
        else if (mediatype == 'lesson') {
            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?_sft_media-types=lesson-video';
                window.location.href = newpage;
            }
            else {
                var newpage = currenturl + '&_sft_media-types=lesson-video';
                window.location.href = newpage;
            }
        }
        else if (mediatype == 'drill') {
            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?_sft_media-types=drill-video';
                window.location.href = newpage;
            }
            else {
                var newpage = currenturl + '&_sft_media-types=drill-video';
                window.location.href = newpage;
            }
        }
        else if (mediatype == 'credit') {
            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?_sft_educational-credit=educational-credit';
                window.location.href = newpage;
            }
            else {
                var newpage = currenturl + '&_sft_educational-credit=educational-credit';
                window.location.href = newpage;
            }
        }
        else {
            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?_sfm_media_type_id=Video';
                window.location.href = newpage;
            }
            else {
                var newpage = currenturl + '&_sfm_media_type_id=Video';
                window.location.href = newpage;
            }
        }*/

        if (mediasort == 'title') {

            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?sort_order=title+asc';
                window.location.href = newpage;
            }
            else {
                if (currenturl.indexOf('sort_order') == -1) {
                    var newpage = currenturl + '&sort_order=title+asc';
                    window.location.href = newpage;
                }
                else {
                    var sortstart = currenturl.indexOf('sort_order');
                    var cutsort = currenturl.slice(0, sortstart);
                    var newpage = cutsort + 'sort_order=title+asc';
                    window.location.href = newpage;
                }
            }

        }
        else if (mediasort == 'presenter') {
            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?sort_order=_sfm_player_presenter+asc+alpha';
                window.location.href = newpage;
            }
            else {
                if (currenturl.indexOf('sort_order') == -1) {
                    var newpage = currenturl + '&sort_order=_sfm_player_presenter+asc+alpha';
                    window.location.href = newpage;
                }
                else {
                    var sortstart = currenturl.indexOf('sort_order');
                    var cutsort = currenturl.slice(0, sortstart);
                    var newpage = cutsort + 'sort_order=_sfm_player_presenter+asc+alpha';
                    window.location.href = newpage;
                }
            }
        }
        else if (mediasort == 'shortlong') {
            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?sort_order=_sfm_video_length+asc+num';
                window.location.href = newpage;
            }
            else {
                if (currenturl.indexOf('sort_order') == -1) {
                    var newpage = currenturl + '&sort_order=_sfm_video_length+asc+num';
                    window.location.href = newpage;
                }
                else {
                    var sortstart = currenturl.indexOf('sort_order');
                    var cutsort = currenturl.slice(0, sortstart);
                    var newpage = cutsort + 'sort_order=_sfm_video_length+asc+num';
                    window.location.href = newpage;
                }
            }
        }
        else if (mediasort == 'longshort') {
            if (lastChar == '/') {
                var newpage = 'http://svr-xi.tennisresources.com/video/' + '?sort_order=_sfm_video_length+desc+num';
                window.location.href = newpage;
            }
            else {
                if (currenturl.indexOf('sort_order') == -1) {
                    alert('no sorting yet');
                    var newpage = currenturl + '&sort_order=_sfm_video_length+desc+num';
                    window.location.href = newpage;
                }
                else {
                    var sortstart = currenturl.indexOf('sort_order');
                    var cutsort = currenturl.slice(0, sortstart);
                    var newpage = cutsort + 'sort_order=_sfm_video_length+desc+num';
                    window.location.href = newpage;
                }
            }
        }
        else {
        }
    });
// END TOP RIGHT "SORT BY DROPDOWN

    } //END if (bodyclass.indexOf('video') != -1)

    //MOBILE SEARCH BUTTON
    jQuery("div.searchlink a").click(function () {
        jQuery("#content #sidebar").addClass('visible');
        jQuery("#graybg").addClass('active');
    });

    jQuery("a.searchclose").click(function () {
        jQuery("#content #sidebar").removeClass('visible');
        jQuery("#graybg").removeClass('active');
    });
    //END MOBILE SEARCH BUTTON

    if (bodyclass.indexOf('home') != -1) {
        window.setTimeout(function () {
            //alert(windowwidth);
            if (windowwidth < 515) {
                jQuery("#banner-search input").attr('placeholder', 'Search...');
            }
            else {
                //alert('this window is big');
            }
        },2000);
    }
});

//ASK IF USER WANTS TO LEAVE PAGE ON CREDIT VIDS
var url = window.location.href;
if (url.indexOf('view=credit') != -1) {

    window.addEventListener("beforeunload", function (e) {
        var confirmationMessage = 'Are you sure you want to leave this page?\nIf you leave before completing the video,\nyou will have to start over to receive credit.';

        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    });
}
//



   