(function($){
	$(document).ready(function(){	

		$("<h4 class='data-filter-section-title'>Search Specific Skills</h4>").insertBefore(".mdf_section_tax_skill input[type='hidden']");

		$(".scrolllink").click(function(e){
			e.preventDefault();
			$('html, body').animate({

		        scrollTop: $("#featured-articles").offset().top

		    }, 1000);

		});

		var slider = $('#loopedSlider');
		slider.find('.slides').delay(4000).fadeTo( "fast", 1, function(){
			$('.preloader-ball').hide();
		});
		

		if(slider.length > 0) {
			
			slider.flexslider({
				prevText: '<span class="fa fa-angle-left"></span>',           
				nextText: '<span class="fa fa-angle-right"></span>',  
				start: function(slider) {
					 $('video').get(0).play();

				},
			});
		}

		$(window).on('load resize', function(){
			var windowWidth = $(this).width(),
			parentMenu = $('#navigation ul.mega-menu > li.mega-menu-item.mega-menu-item-has-children');
			if( windowWidth < 1023 ) {
				if(!parentMenu.hasClass('has-child')){
					parentMenu.addClass('has-child')
					$('.has-child').append( "<span class='dropdown-btn'><i class='fa fa-angle-right'></i></span>" );
					$('.dropdown-btn').click(function(){
						$(this).siblings('.mega-sub-menu').slideToggle();
					});
				}
			} else {
				parentMenu.removeClass('has-child');
				$('.dropdown-btn').remove();
			}


	
		});

		var list_children = $('#sidebar .widget ul li')
								.has( 'ul' )
									.addClass('has-children')
										.append( "<span class='dropdown-btn-sidebar'><i class='fa fa-sort-asc'></i></span>" );

		$('.dropdown-btn-sidebar').click(function(){
			$(this).siblings('ul').stop().slideToggle();
		});
		$("<select />").appendTo("#footer-widgets .widget_nav_menu > div");

		// Create default option "Go to..."
		$("<option />", {
		   "selected": "selected",
		   "value"   : "",
		   "text"    : "Go to..."
		}).appendTo("#footer-widgets .widget_nav_menu > div select");

		// Populate dropdown with menu items
		$("#footer-widgets .widget_nav_menu > div a").each(function() {
		 var el = $(this);
		 $("<option />", {
		     "value"   : el.attr("href"),
		     "text"    : el.text()
		 }).appendTo("#footer-widgets .widget_nav_menu > div select");
		});

		$("#footer-widgets .widget_nav_menu > div select").change(function() {
		  window.location = $(this).find("option:selected").val();
		});

		$('.search_main input.s').focusin(function(){
			$(this).parent().parent().addClass('focusin');	
		});
		$('.search_main input.s').focusout(function(){
			$(this).parent().parent().removeClass('focusin');	
		});

		$('.featured-articles .widget').last().addClass('last');
	});
})(jQuery);