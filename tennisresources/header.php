<?php
/**
 * Header Template
 *
 * Here we setup all logic and XHTML that is required for the header section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
<title><?php woo_title(); ?></title>
<?php woo_meta(); ?>
<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>" />
<?php wp_head(); ?>
<?php woo_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php woo_top(); ?>
<div id="wrapper">

	<div id="inner-wrapper">
	<div id="join-pop" class="modal" style="text-align:center; display:none;">
	<h3>Select a Subscription Type</h3>
	<p><a class="button" href="https://customer.uspta.org/PROCESSOR/usptamembers/store/item_detail.aspx?iproductcode=TRCSFULL" target="_blank">ANNUAL SUBSCRIPTION</a><br /><br /><a class="button" href="https://customer.uspta.org/PROCESSOR/usptamembers/store/item_detail.aspx?iproductcode=TRCSQTR" target="_blank">QUARTERLY SUBSCRIPTION</a></p>
	<p>You will now be taken to the USPTA web site to complete your purchase.  If you do not have a "My USPTA" account, you will be asked to create one.  This is the same account you will use to login here at TennisResources.com.  After you have completed your purchase, come back to the TennisResources.com web site and log in.</p>
</div>
	<?php if (!is_front_page() && !is_page(array('for-player','for-trainers'))) { ?>
	<div class="searchlink"><a class="button" href="#">SEARCH</a></div>
	<?php } ?>
<div id="graybg"></div>

	<?php woo_header_before(); ?>

	<header id="header" class="col-full">

		<?php woo_header_inside(); ?>

	</header>
	<?php woo_header_after(); ?>