<?php
/**
 * Video Content Template
 *
 * This template is the default page content template. It is used to display the content of the
 * `single.php` template file, contextually, as well as in archive lists or search results.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */
if ( ! defined( 'ABSPATH' ) ) 
	exit;

	$player = get_post_meta($post->ID, player_presenter, true);
	$vidtime = get_post_meta($post->ID, video_length, true);
	$vidurl = get_post_meta($post->ID, video_file_url, true);

	$videoid = get_post_meta($post->ID, credit_video_id, true);

	$taxargs = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names');
	
	$access = wp_get_post_terms( $post->ID, 'access', $taxargs);

if (is_single()) {

	require_once( MIGUEL_PLUGIN_DIR_SRC . 'includes/TRCWCFSvc.php' );

	if (is_user_logged_in()) {

		$user_ID = get_current_user_id();

		$memberid = get_user_meta($user_ID, member_id, true);
		
		if (empty($memberid)) {
			$memberid = 0;
		}
	} 
	else {
		$memberid = 0;
	}


	$result = TRCWCFSvc::getUserSubscriptions(array('memberID' => $memberid));		if( $result->success === false ) {			}	else {		$subItems = array();
		foreach ($result->data->Subscriptions->ItemSubscription as $subscription) {
			$item = $subscription->itemCode;
			array_push($subItems, $item);
		}	$arraytest = in_array($videoid, $subItems);	}	}

$settings = array(
				'thumb_w' => 300,
				'thumb_h' => 200,
				'thumb_size' => 'medium',
				'thumb_align' => 'aligncenter',
				'post_content' => 'excerpt',
				'comments' => 'both'
				);
if ($access[0] == "Members") {
$memlock = '<i class="fa fa-lock red"></i>';
}
if ($access[0] == "Pay Per Video") {
$memlock = '<i class="fa fa-shopping-cart yellow"></i>';
}
if ($access[0] == "Free") {
$memlock = '<i class="fa fa-unlock green"></i>';
}
if (is_single()) {

if ($access[0] == "Members" && !$memberid == 0) {
$vidcode = do_shortcode('[KGVID width="640" height="480" view_count="true"]' . $vidurl . '[/KGVID]');
}
if ($access[0] == "Members" && !is_user_logged_in()) {
$vidcode = '<div class="nonmember"><p>Ready to learn?  Let\'s get started!<br /><a href="/login">Log in</a> or <a href="/register">Sign up</a> today!</p></div>';
}
if ($access[0] == "Members" && $memberid == 0) {
$vidcode = '<div class="nonmember"><p>Ready to learn?  Let\'s get started!<br /><a href="/login">Log in</a> or <a href="/register">Sign up</a> today!</p></div>';
}
if ($access[0] == "Free") {
$vidcode = do_shortcode('[KGVID width="640" height="480" view_count="true"]' . $vidurl . '[/KGVID]');
}if ($access[0] == "Pay Per Video" && !is_user_logged_in()) {
$vidcode = '<div class="nonmember"><p>This video is available to purchase<br /><a href="#">Click here to buy now!</a></p></div>';
}if ($access[0] == "Pay Per Video" && $result->success === false) {
$vidcode = '<div class="nonmember"><p>This video is available to purchase<br /><a href="#">Click here to buy now!</a></p></div>';
}if ($access[0] == "Pay Per Video" && $arraytest === false) {
$vidcode = '<div class="nonmember"><p>This video is available to purchase<br /><a href="#">Click here to buy now!</a></p></div>';
}
if ($access[0] == "Pay Per Video" && $arraytest === true) {
$vidcode = do_shortcode('[KGVID width="640" height="480" view_count="true"]' . $vidurl . '[/KGVID]');
}}
$settings = woo_get_dynamic_values( $settings );

$title_before = '<h1 class="title entry-title">';
$title_after = '</h1>';

if ( ! is_single() ) {
$title_before = '<h2 class="looptitle">';
$title_after = '</h2>';
$title_before = $title_before . '<a href="' . esc_url( get_permalink( get_the_ID() ) ) . '" rel="bookmark" title="' . the_title_attribute( array( 'echo' => 0 ) ) . '">';
$title_after = '</a>' . $title_after;
}

$page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

woo_post_before();
?>
<article <?php post_class(); ?>>
<?php
woo_post_inside_before();
$postthumb = woo_image( 'width=300&height=200&class=size-medium aligncenter');

if ( 'content' != $settings['post_content'] && ! is_singular() )
	/*woo_image( 'width=' . esc_attr( $settings['thumb_w'] ) . '&height=' . esc_attr( $settings['thumb_h'] ) . '&class=thumbnail ' . esc_attr( $settings['thumb_align'] ) );*/
	echo '<div class="postthumb">' . $postthumb . '<div class="vidtime"><span class="vidlength">' .  $vidtime . '</span><span class="vidaccess">' . $memlock . '</span></div></div>';
?>

<?php $vidtitle = the_title( $title_before, $title_after ); ?>
	<header>
	<?php echo $vidtitle . '<div class="vidmeta">by ' . $player . '</div>';  ?>
	</header>
<?php
woo_post_meta();
?>
	<section class="entry">
<?php
if (is_single()) { echo '<div>' . $vidcode . '</div><div style="clear:both"></div>';}
if ( 'content' == $settings['post_content'] || is_single() ) { the_content( __( 'Continue Reading &rarr;', 'woothemes' ) ); } else { the_excerpt(); }
if ( 'content' == $settings['post_content'] || is_singular() ) wp_link_pages( $page_link_args );
?>
	</section><!-- /.entry -->
	<div class="fix"></div>
<?php
woo_post_inside_after();
?>
</article><!-- /.post -->
<?php
woo_post_after();
$comm = $settings['comments'];
if ( ( 'post' == $comm || 'both' == $comm ) && is_single() ) { comments_template(); }
?>