<?php  

	class Tennis_Resources_Theme {



		public $child_dir;



		public function __construct() {
			$this->child_dir = get_stylesheet_directory_uri();
			add_action('wp_enqueue_scripts', array($this, 'enqueueStyle'));
			add_action('wp_enqueue_scripts', array($this, 'enqueueScript'));
			add_action('widgets_init', array($this, 'registerSidebar'));
			add_image_size( 'videos-image', 106, 65, true); 
		}



		public function registerStyle() {

			wp_register_style('parent-style', get_template_directory_uri() . '/style.css');
			wp_register_style('open-sans-font', 'https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700', array('parent-style'), '1.0.0', 'all');
			wp_register_style('raleway-font', 'https://fonts.googleapis.com/css?family=Raleway:400,400italic,600,700', array('parent-style'), '1.0.0', 'all');
			wp_register_style('montserrat-font', 'https://fonts.googleapis.com/css?family=Montserrat:400,400italic,600,700', array('parent-style'), '1.0.0', 'all');
			wp_register_style('custom-style', $this->child_dir . '/style.css', array('parent-style'), '1.0.0', 'all');
			wp_register_style('modal-style', $this->child_dir . '/styles/jquery.modal.css', array('parent-style'), '1.0.0', 'all');
			wp_register_style('child-style', $this->child_dir . '/styles/sass/stylesheets/style.css', array('parent-style'), '1.0.0', 'all');
		}



		public function enqueueStyle() {
			$this->registerStyle();

			wp_enqueue_style('parent-style');
			wp_enqueue_style('open-sans-font');
			wp_enqueue_style('raleway-font');
			wp_enqueue_style('montserrat-font');
			wp_enqueue_style('custom-style');
			wp_enqueue_style('modal-style');
			wp_enqueue_style('child-style');



		}



		public function registerScript() {
			wp_register_script('main-js', $this->child_dir . '/js/main.js', array('jquery'), '1.0.0', true);
			wp_register_script('parseparams-js', $this->child_dir . '/js/jquery.parseparams.js', array('jquery'), '1.0.0', true);
			wp_register_script('cookie-js', $this->child_dir . '/js/js.cookie.js', array('jquery'), '1.0.0', true);
			wp_register_script('modal-js', $this->child_dir . '/js/jquery.modal.min.js', array('jquery'), '1.0.0', true);
			wp_register_script('searchscripts-js', $this->child_dir . '/js/searchscripts.js', array('jquery'), '1.0.0', true);
		}

		public function enqueueScript() {
			$this->registerScript();
			wp_enqueue_script('main-js');
			wp_enqueue_script('parseparams-js');
			wp_enqueue_script('cookie-js');
			wp_enqueue_script('modal-js');
			wp_enqueue_script('searchscripts-js');
		}

		public function registerSidebar() {

			register_sidebar(	array(
				'name'			=>	'How does it works',
				'id'			=>	'how-does-it-works',
				'description' 	=> 	'',
				'before_widget' =>	'<div id="%1$s" class="widget %2$s threecol-one">',
				'after_widget'	=> 	'</div>',
				'before_title'	=>	'<h3 class="widget-title">',
				'after_title'	=>	'</h3>'
			));

			register_sidebar(	array(
				'name'			=>	'Facebook Comment Box',
				'id'			=>	'facebook-comment-box',
				'description' 	=> 	'',
				'before_widget' =>	'<div id="%1$s" class="widget %2$s">',
				'after_widget'	=> 	'</div>',
				'before_title'	=>	'<h3 class="featured-section-title">',
				'after_title'	=>	'</h3>'
			));

			register_sidebar(	array(
				'name'			=>	'Footer Ads',
				'id'			=>	'ads',
				'description' 	=> 	'This Sidebar Widget will show up on the home below the Featured Articles',
				'before_widget' =>	'<div id="%1$s" class="widget %2$s">',
				'after_widget'	=> 	'</div>',
				'before_title'	=>	'<h3 class="ads-title">',
				'after_title'	=>	'</h3>'
			));

		}
	}



	include_once( get_stylesheet_directory() .'/includes/custom-biz-slider.php');



	add_action('after_setup_theme', function(){
		include_once( get_stylesheet_directory() .'/includes/theme-hooks.php');
		include_once( get_stylesheet_directory() .'/includes/theme-shortcode.php');
		include_once( get_stylesheet_directory() .'/includes/theme-option.php');
		$theme = new Tennis_Resources_Theme();
	});



/* Change Archive Template H1 */



add_filter( 'woo_archive_title', 'new_woo_archive_title' );
	function new_woo_archive_title () {
	$category = single_cat_title("", false); // don't show a prefix for category title and return it for use in PHP instead of displaying
	$new_title = '<h1 class="archive_header">'. $category .'</h1>'; // now display the category only
	return $new_title;

} // End filter



function lxb_change_widget_title($title)

{

    //convert square brackets to angle brackets

    $title = str_replace('[', '<', $title);

    $title = str_replace(']', '>', $title);

    //strip tags other than the allowed set

    $title = strip_tags($title, '<a><blink><br><span>');

    return $title;

}

add_filter('widget_title', 'lxb_change_widget_title');

function custom_excerpt_length( $length ) {

	return 10;

}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


add_filter('relevanssi_get_words_query', 'fix_query');
function fix_query($query) {
    $query = $query . " HAVING c > 1";
    return $query;
}

/*add_filter('relevanssi_didyoumean_url', 'rlv_modify_url');
function rlv_modify_url($url) {
    $url = get_bloginfo('url') . '/video/';
    $url = esc_attr(add_query_arg(array(
        '_sf_s' => urlencode($closest)
        ), $url ));
    return $url;
}*/

require( 'trapi_functions.php' );