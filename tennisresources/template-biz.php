<?php

/**

 * Template Name: Business

 *

 * The business page template displays your posts with a "business"-style

 * content slider at the top.

 *

 * @package WooFramework

 * @subpackage Template

 */



global $woo_options, $wp_query;

get_header();

$page_template = woo_get_page_template();

?>

  <!-- #content Starts -->



	<?php woo_content_before(); ?>



	<?php if ( ( isset( $woo_options['woo_slider_biz'] ) && 'true' == $woo_options['woo_slider_biz'] ) && ( isset( $woo_options['woo_slider_biz_full'] ) && 'true' == $woo_options['woo_slider_biz_full'] ) ) { $saved = $wp_query; woo_slider_biz(); $wp_query = $saved; } ?>

    <div id="content" class="col-full business">

    	<div id="main-sidebar-container">

            <!-- #main Starts -->

            <?php woo_main_before(); ?>

	<?php if ( ( isset( $woo_options['woo_slider_biz'] ) && 'true' == $woo_options['woo_slider_biz'] ) && ( isset( $woo_options['woo_slider_biz_full'] ) && 'false' == $woo_options['woo_slider_biz_full'] ) ) { $saved = $wp_query; woo_slider_biz(); $wp_query = $saved; } ?>

            <section id="main">

<?php

	woo_loop_before();

	if ( have_posts() ) { $count = 0;

		while ( have_posts() ) { the_post(); $count++;

			woo_get_template_part( 'content', 'page-template-business' ); // Get the page content template file, contextually.

		}

	}

	woo_loop_after();

?>

            </section><!-- /#main -->

            <?php woo_main_after(); ?>

			<?php get_sidebar(); ?>

		</div><!-- /#main-sidebar-container -->

		<?php get_sidebar( 'alt' ); ?>

    </div><!-- /#content -->

	<?php woo_content_after(); 

    wp_reset_query();

    

    $section1_content = get_field('section_1_content');  

    $section2_content = get_field('section_2_content'); 

    $section1_background = get_field('section_1_background');  

    $section2_background = get_field('section_2_background'); 



    if(!empty( $section1_content )):

      echo '<section class="featured-articles" id="featured-articles"><div class="col-full">';

        echo  $section1_content;
        echo '<div class="fix"></div>';
        $how_does_it_work = '#how-does-it-works';

        if ( woo_active_sidebar( $how_does_it_work ) ) {

          woo_sidebar( $how_does_it_work );

        }
      echo '</div></section>';

    endif;



    if( !empty($section2_content) ):

      echo '<section class="featured-articles2" style="background-image: url('.$section2_background['url'].')"><div class="col-full">';

         echo  $section2_content;

      echo '<div class="phone">Phone</div></div></section>';

    endif;



  ?>

	<section class="featured-section">

		<div class="col-full">

			<section class="featured-video fivecol-three">

        <?php  

          $fv_title =  get_option('woo_xo_tenr_fv_title');

          if(!empty($fv_title)) {

            echo '<h2>'.$fv_title .'</h2>';

          }

        ?>

        <div class="featured-video-list">



          <?php    

              $args = array(

                'post_type' => 'video',

		'meta_key' => 'featured',

		'meta_value' => 'Yes'

		);

              $videosPost = new WP_Query($args); 

              if($videosPost->have_posts()):

                while($videosPost->have_posts()): $videosPost->the_post();



          ?>



          <div class="video-item">



              <div class="fourcol-three">



                  <?php 



                    $time = get_field('video_length'); 



                    $player = get_field('player_presenter');



                    $dvd = get_field('buy_now_url');







                  ?>



                  <?php   the_title('<h4><a href="'.get_permalink().'">', '</a> ('.$time.')</h4>'); ?>



                  <div class="fourcol-one">



                      <?php 

			if (!empty(get_the_post_thumbnail(get_the_ID(), 'videos-image'))) {

			echo '<a href="'.get_permalink().'">'.get_the_post_thumbnail(get_the_ID(), 'videos-image').'</a>'; 

			}

			else {

			echo '<a href="'.get_permalink().'"><img src="/wp-content/uploads/2015/12/defaultthumb.jpg" width="97" height="65" /></a>'; 

			}?>



                  </div>



                  <div class="fourcol-three last"> 



                      <?php   echo '<p>'.get_the_excerpt().' ('.$player.')</p>'; ?>



                  </div>



                  <div class="clear"> </div>



              </div>



              <div class="fourcol-one last right-section">



                  <?php //if(function_exists('the_ratings')) { the_ratings(); } ?>



                  <?php echo !empty($dvd) ? ' <p><a target="_blank" href="'.$dvd.'" class="buy-on-dvd">BUY ON DVD</a></p>': ''; ?>



                 



              </div>



                <div class="clear"> </div>



          </div>  



          <?php    

            endwhile;

            endif;

            wp_reset_query();

          ?>



        </div> 



      </section>



			<aside class="facebook-comment fivecol-two last">

        <?php  

          $facebook_comment = '#facebook-comment-box';

          if(woo_active_sidebar($facebook_comment)):

            woo_sidebar( $facebook_comment  );

          endif;

        ?>

      </aside>

		</div>

	</section>

  



<?php get_footer(); ?>