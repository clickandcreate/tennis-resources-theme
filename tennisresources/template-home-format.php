<?php
/**
 * Template Name: Home Format
 *
 * The business page template displays your posts with a "business"-style
 * content slider at the top.
 *
 * @package WooFramework
 * @subpackage Template
 */

global $woo_options, $wp_query;
get_header();
$page_template = woo_get_page_template();
?>
  <!-- #content Starts -->
    <?php  

    $section1_content = get_field('section_1_content');  
    $section2_content = get_field('section_2_content'); 
    $section1_background = get_field('section_1_background');  
    $section2_background = get_field('section_2_background'); 
    $testimonial_content = get_field('testimonial_section'); 

    $hero_img = get_field('hero_banner_img');  
    $hero_banner_content = get_field('hero_banner_content'); 

    if(!empty($hero_img)) {
      echo '<div class="hero-banner"><div class="hero-image" style="background-image: url('.$hero_img['url'].')"></div><div class="content"><div class="col-full">'.do_shortcode($hero_banner_content).'
      </div><div class="downarrow"><a class="scrolllink" href="#featured-articles"><i class="fa fa-angle-down"></i></a></div></div></div>';
    }
    if(!empty( $section1_content )):
      echo '<section class="featured-articles" id="featured-articles" style="background-image: url('.$section1_background['url'].')"><div class="col-full">';
        echo  $section1_content;
      echo '</div></section>';
    endif;

    if( !empty($section2_content) ):
      echo '<section class="featured-articles2" style="background-image: url('.$section2_background['url'].')"><div class="col-full">';
         echo  $section2_content;
      echo '<div class="phone">Phone</div></div></section>';
    endif;
  ?>

  <?php if(!empty($testimonial_content)): ?>
	<section class="testimonial-section">
		<div class="col-full">
	     <?php  
          echo $testimonial_content;
       ?>
		</div>
	</section>
<?php endif; ?>

<?php get_footer(); ?>