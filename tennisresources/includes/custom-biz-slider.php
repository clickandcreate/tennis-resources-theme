<?php 
if ( ! function_exists( 'woo_slider_biz_view' ) ) {
	function woo_slider_biz_view( $args = null, $slides = null ) {

		global $woo_options, $post;

		// Default slider settings.
		$defaults = array(
							'id' => 'loopedSlider',
							'width' => '960',
							'container_css' => '',
							'slide_styles' => ''
						);

		// Merge the arguments with defaults.
		$args = wp_parse_args( $args, $defaults );

		// Init slide count
		$count = 0;

		$loading_background = $woo_options['woo_xo_tenr_loading_bg'];
		$loading_img = !empty($loading_background) ? 'style="background-image:url('.$loading_background.')"' : '';

	?>

	<?php do_action('woo_biz_slider_before'); ?>

	<div id="<?php echo esc_attr( $args['id'] ); ?>" <?php echo $loading_img ?> <?php if ( '' != $args['container_css'] ): ?> class="<?php echo esc_attr( $args['container_css'] ); ?>"<?php endif; ?><?php if ( !apply_filters( 'woo_slider_autoheight', true ) ): ?> style="height: <?php echo apply_filters( 'woo_slider_height', 350 ); ?>px;"<?php endif; ?>>
		<div class="preloader-ball"></div>
		<ul class="slides"<?php if ( !apply_filters( 'woo_slider_autoheight', true ) ): ?> style="height: <?php echo apply_filters( 'woo_slider_height', 350 ); ?>px;"<?php endif; ?>>

			<?php foreach ( $slides as $k => $post ) { setup_postdata( $post ); $count++; ?>

			<?php
				// Slide Styles
				if ( $count >= 2 ) { $args['slide_styles'] .= ' display:none;'; } else { $args['slide_styles'] = ''; }
			?>

			<li id="slide-<?php echo esc_attr( $post->ID ); ?>" class="slide slide-number-<?php echo esc_attr( $count ); ?>" <?php if ( '' != $args['slide_styles'] ): ?>style="<?php echo esc_attr( $args['slide_styles'] ); ?>"<?php endif; ?>>
				<div class="ovelay-background-banner"></div>
				<?php
					$type = woo_image('return=true');
					if ( $type ):
						$url = get_post_meta( $post->ID, 'url', true );
				?>

					
					<?php// woo_image( 'width=' . $args['width'] . '&link=img&noheight=true' ); ?>
					<div class="biz-slider-image" >
						<video autoplay loop muted>
						  <source src="<?php echo $url ?>" type="video/mp4">
						  <source src="<?php echo $url ?>" type="video/ogg">
						  Your browser does not support the video tag.
						</video>

					</div>
					

					<?php if ( 'true' == $woo_options['woo_slider_biz_title'] || '' != get_the_excerpt() ): ?>
					<div class="content">
						<div class="col-full">
							
						<?php if ( 'true' == $woo_options['woo_slider_biz_title'] ): ?>
						<div class="title">
							<h2 class="title">
								<?php the_title(); ?>
							</h2>
						</div>
						<?php endif; ?>

	
						<div class="excerpt">
							<?php
		 					// 	$content = $post->post_excerpt;
								// $content = do_shortcode( $content );
								// echo wpautop( $content );
								echo do_shortcode(get_the_content());
							?>
						</div><!-- /.excerpt -->
						</div>
						<div class="downarrow"><a class="scrolllink" href="#featured-articles">Learn More<i class="fa fa-angle-down"></i></a></div>
					</div><!-- /.content -->
					<?php endif; ?>

				<?php else: ?>

					<section class="entry col-full">
						<?php the_content(); ?>
					</section>

				<?php endif; ?>

			</li><!-- /.slide-number-<?php echo esc_attr( $count ); ?> -->

			<?php } // End foreach ?>

			<?php wp_reset_postdata();  ?>

		</ul><!-- /.slides -->
		<span class="arrow-down"></span>
	</div><!-- /#<?php echo $args['id']; ?> -->

	<?php if ( isset( $woo_options['woo_slider_pagination'] ) && $woo_options['woo_slider_pagination'] == 'true' ) : ?>
		<div class="pagination-wrap slider-pagination">
			<ol class="flex-control-nav flex-control-paging">
				<?php for ( $i = 0; $i < $count; $i++ ): ?>
					<li><a><?php echo ( $i + 1 ) ?></a></li>
				<?php endfor; ?>
			</ol>
		</div>
	<?php endif; ?>

	<?php do_action('woo_biz_slider_after'); ?>

<?php
	} // End woo_slider_biz_view()
}
