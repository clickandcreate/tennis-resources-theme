<?php 


function woo_options_add( $options ) {

	$shortname = "woo_xo";

	//Access the WordPress Pages via an Array
	$woo_pages = array( '0' => 'Select a page:');
	$woo_pages_obj = get_pages('sort_column=post_parent,menu_order');
	foreach ($woo_pages_obj as $woo_page) {
	    $woo_pages[$woo_page->ID] = $woo_page->post_title; }



	$new_options[] = array( "name" => __( 'Social Links', 'woothemes' ),
				"icon" => "general",
                "type" => "heading");

        $new_options[] = array( "name" => __( 'Facebook', 'woothemes' ),
					"desc" => __( 'Enter Link here', 'woothemes' ),
					"id" => $shortname."_tenr_facebook",
					"type" => "text");

        $new_options[] = array( "name" => __( 'Twitter', 'woothemes' ),
					"desc" => __( 'Enter Link here', 'woothemes' ),
					"id" => $shortname."_tenr_twitter",
					"type" => "text");

        $new_options[] = array( "name" => __( 'Google Plus', 'woothemes' ),
					"desc" => __( 'Enter Link here', 'woothemes' ),
					"id" => $shortname."_tenr_google-plus",
					"type" => "text");

        $new_options[] = array( "name" => __( 'Linkedin', 'woothemes' ),
					"desc" => __( 'Enter Link here', 'woothemes' ),
					"id" => $shortname."_tenr_linkedin",
					"type" => "text");

    $new_options[] = array( "name" => __( 'Header Content', 'woothemes' ),
				"icon" => "general",
                "type" => "heading");

    	$new_options[] = array( "name" => __( 'Join', 'woothemes' ),
					"desc" => __( 'Enter Link here', 'woothemes' ),
					"id" => $shortname."_tenr_hdr_join",
					"type" => "text");

    	$new_options[] = array( "name" => __( 'Login', 'woothemes' ),
					"desc" => __( 'Enter Link here', 'woothemes' ),
					"id" => $shortname."_tenr_hdr_login",
					"type" => "text");

    	$new_options[] = array( "name" => __( 'USPTA', 'woothemes' ),
					"desc" => __( 'Upload Image', 'woothemes' ),
					"id" => $shortname."_tenr_uspta_logo",
					"type" => "upload");

		$new_options[] = array( "name" => __( 'Featured Video Section', 'woothemes' ),
				"icon" => "general",
                "type" => "heading");

			$new_options[] = array( "name" => __( 'Title Section', 'woothemes' ),
					"desc" => __( 'Enter text here', 'woothemes' ),
					"id" => $shortname."_tenr_fv_title",
					"type" => "text");

		$new_options[] = array( "name" => __( 'Banner Background', 'woothemes' ),
				"icon" => "general",
                "type" => "heading");

			$new_options[] = array( "name" => __( 'Loading Background Image', 'woothemes' ),
					"desc" => __( 'Upload Image', 'woothemes' ),
					"id" => $shortname."_tenr_loading_bg",
					"type" => "upload");

		$options = wp_parse_args($new_options, $options);

	return $options;
}
