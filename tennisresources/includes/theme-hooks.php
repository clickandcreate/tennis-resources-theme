<?php 


	add_action('wp_footer', 'lightbox_background');
	function lightbox_background() {
		echo '<div class="lighbox-overlay"><div>';
	}

	add_action('woo_footer_top', 'woo_custom_ads_widget', 5);

	function woo_custom_ads_widget() {

		$ads = '#ads';

		if(woo_active_sidebar($ads)):

	    echo '  <section class="featured-advetise-slide">

	    <div class="col-full">';

				woo_sidebar( $ads  );

	    echo '</div>

	    </section>';

		endif;

	}



	add_action('woo_content_before', 'custom_inner_page_banner');

	function custom_inner_page_banner() {

		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );

		$custom_img = $thumb['0']; 

		$default_img = get_stylesheet_directory_uri().'/images/inner-banner.jpg';

		$banner_img =  $default_img;



		echo !is_front_page() ?  '<div class="inner-page-banner" style="background-image: url('.$banner_img.');">

			<div class="col-full">

			<div id="main-sidebar-container">

			<div id="sidebar">&nbsp;</div>

			<div id="main">

			<h2 class="custom-page-title">'.get_the_title().'</h2></div></div>

			</div>

		</div>': '';



	}

	add_action('woo_header_before', 'woo_custom_header_links', 20);

	function woo_custom_header_links() {



		global $woo_options;

		$join_link = get_option('woo_xo_tenr_hdr_join');

		$login_link = get_option('woo_xo_tenr_hdr_login');

		$ustpa = get_option('woo_xo_tenr_uspta_logo');

		global $current_user;

      		get_currentuserinfo();



		echo '<section class="top-header"><div class="col-full">';

		echo '<div class="left-content twocol-one">&nbsp;</div>';

		echo '<div class="right-content twocol-one last">';

			echo '<ul class="meta-links">';

				echo !empty($join_link) && !is_user_logged_in() ? '<li class="active-btn"><a class="joinlink" href="'.$join_link.'" rel="modal:open">JOIN</a></li>' : '';

				echo !empty($login_link) && !is_user_logged_in() ? '<li><a href="'.$login_link.'">LOGIN</a></li>' : '';

				echo !empty($login_link) && is_user_logged_in() ? '<li class="active-btn"><a href="/my-account">Hello ' . $current_user->first_name . '</a></li><li><a href="/my-account">MY ACCOUNT</a></li><li><a href="'. get_permalink(6593) . '">LOG OUT</a></li>' : '';

				echo  '<li class="search"><a href="'.$login_link.'"><img src="'.$ustpa.'" alt="uspta"></a></li>';

			echo '</ul>';

		echo '</div>';

		echo '</div></section>';

	}



	add_action( 'init', 'woo_custom_move_navigation', 10 );

	function woo_custom_move_navigation () {

	    //remove navigation from the current position

	     remove_action( 'woo_header_after','woo_nav', 10 );

	    // Add main nav to the woo_header_inside hook

	    add_action( 'woo_header_inside','woo_nav', 10 );

	} // End woo_custom_move_navigation()

	

	// CHANGE WORDPRESS LOGIN LOGO

	function my_login_logo() { ?>

	    <style type="text/css">

	        .login h1 a {

	            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);

	            padding-bottom: 0px;

				width: 380px;

				height: 62px;

				background-size: contain;

				margin-left: -30px;



	        }

	    </style>

	<?php }

	add_action( 'login_enqueue_scripts', 'my_login_logo' );



	// CHANGE WORDPRESS LOGIN LOGO URL

	function my_login_logo_url() {

	    return home_url();

	}

	add_filter( 'login_headerurl', 'my_login_logo_url' );



	// CHANGE WORDPRESS LOGIN LOGO TITLE

	function my_login_logo_url_title() {

	    return get_bloginfo('name');

	}

	add_filter( 'login_headertitle', 'my_login_logo_url_title' );