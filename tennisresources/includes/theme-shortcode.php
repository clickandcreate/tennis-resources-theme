<?php 

	add_shortcode('social_links', 'custom_tenr_social_links');
	function custom_tenr_social_links() {
		global $woo_options;

		$prefix = 'woo_xo_tenr_';
		$socials = array();
		$socials['facebook'] = get_option($prefix.'facebook');
		$socials['twitter'] = get_option($prefix.'twitter');
		$socials['linkedin'] = get_option($prefix.'linkedin');

		$social_html = '<div class="tenr-social-links">';
		foreach($socials as $social => $val) {
			$social_html .= !empty($val) ? '<a href="'.esc_url($val).'" title="'.$social.'"><i class="fa fa-'.$social.'-square"></i></a>' : '';
		}
		$social_html .=	 '</div>';

		return $social_html;
	}

	function wpbsearchform( $form ) {

	    $form = '<form role="search" method="get" id="banner-search" action="' . home_url( '/video/' ) . '" >
	    <input type="text" value="' . get_search_query() . '" name="_sf_s" id="s" placeholder="Search for your Resource..."/>
	    <button type="submit" id="searchsubmit" ><i class="fa fa-search"></i></button>

	    </form>';

	    return $form;
	}

	add_shortcode('wpbsearch', 'wpbsearchform');