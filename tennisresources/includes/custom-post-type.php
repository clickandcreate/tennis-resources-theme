<?php
/*
 *Program Name : Custom Post Type
 */

/*
 * Creating a post type. End this comment to enable */

class CustomPostType {
	public $support_editors;
	public $name;
	public function __construct($input) {
		$this->name = $input;
		$this->support_editors = array('title','editor','excerpt','thumbnail','custom-fields');

		add_action('init', array($this, 'add_custom_post'));
		add_action('init', array($this, 'add_custom_category'));

	}

	public function add_custom_post() {
		foreach ($this->name as $cptname ) {
			# code...
			$single = $cptname;
			$labels = array(
							'name' => __($cptname,'cnc'),
							'singular_name' => __($single,'cnc'),
							'add_new' => __('Add New '.$single,'cnc'),
							'add_new_item' => __('Add New Item to '.$cptname,'cnc'),
							'edit' => __('Edit '.$cptname,'cnc'),
							'edit_item' => __('Edit '.$cptname,'cnc'),
							'new_item' => __('New '.$cptname.' item','cnc'),
							'view' => __('View '.$cptname,'cnc'),
							'view_item' => __('View '.$cptname,'cnc'),
							'search_item' => __('Search Items to '.$cptname,'cnc'),
							'not_found' => __('No item found in '.$cptname,'cnc'),
							'not_found_in_trash' => __('No '.$cptname.' item found in Trash','cnc'),
							'parent' => __('Parent '.$cptname,'cnc')
							);

			$args = array(
						  'labels' => $labels,
						  'description' => __($cptname.' custom post type.','cnc'),
						  'show_ui' => true,
						  'exclude_from_search' =>false,
						  'supports' => $this->support_editors,
						  'public' => true,
						  'has_archive' => true
						  );
			$cpt_name = str_replace(' ', '', strtolower($cptname));
			register_post_type($cpt_name, $args);		
		}	 
	}

	public function add_custom_category() {
		foreach ($this->name as $cptcat ) {
			$single = substr($cptcat, 0, -1);
			$labels = array(
			    'name'              => _x( $cptcat.' Categories', 'taxonomy general name' ),
			    'singular_name'     => _x( $single.' Category', 'taxonomy singular name' ),
			    'search_items'      => __( 'Search'.$cptcat.' Categories' ),
			    'all_items'         => __( 'All '.$cptcat.' Categories' ),
			    'parent_item'       => __( 'Parent '.$cptcat.' Category' ),
			    'parent_item_colon' => __( 'Parent '.$cptcat.' Category:' ),
			    'edit_item'         => __( 'Edit '.$cptcat.' Category' ), 
			    'update_item'       => __( 'Update '.$cptcat.' Category' ),
			    'add_new_item'      => __( 'Add New '.$cptcat.' Category' ),
			    'new_item_name'     => __( 'New '.$cptcat.' Category' ),
			    'menu_name'         => __( $cptcat.' Categories' ),
			 );
			$args = array(
				'labels' => $labels,
				'hierarchical' => true,
			);
			$cat_name = str_replace(' ', '', strtolower($cptcat));
			// register_taxonomy( $cat_name.'_category', $cat_name , $args );
			$rewrite = array('slug' => $cat_name.'-category','with_front' =>false);
			register_taxonomy ( $cat_name.'_category',$cat_name,
								array('labels' => $labels,
								'hierarchical' => true,
								'public' => true,
								'show_ui' => true,
								'query_var' => $cat_name.'-category',
								'show_tagcloud' => true,
								'rewrite' => $rewrite,
								'label' =>$cat_name.'-category'));		
		}
	}
}

$cpt = new CustomPostType(array('Featured Videos'));

?>
