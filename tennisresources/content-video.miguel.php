<?php
/**
 * Video Content Template
 *
 * This template is the default page content template. It is used to display the content of the
 * `single.php` template file, contextually, as well as in archive lists or search results.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */

$auth = apply_filters( 'trapi_authorize_user_to_video', $post->ID );
if( empty( $auth ) )
	die('Access denied. Reason unknown');

if( $auth->authorized === false && ($auth->access_level === 'Need Video' || 
									$auth->access_level === 'Need Level' || 
									$auth->access_level === 'Need MemberID' || 
									$auth->access_level === 'Invalid Level') ) {

	die( $auth->access_level );
}

if( $auth->access_level === "Members" ) {

	$memlock = '<i class="fa fa-lock red"></i>';	

	if( ! $auth->authorized ) {

		$vidcode = '<div class="nonmember"><p>Ready to learn?  Let\'s get started!<br /><a href="/login">Log in</a> or <a href="/register">Sign up</a> today!</p></div>';
	}
}

if( $auth->access_level === "Pay Per Video" ) {

	$memlock = '<i class="fa fa-shopping-cart yellow"></i>';

	if( ! $auth->authorized ) {
		
		$vidcode = '<div class="nonmember"><p>This video is available to purchase<br /><a href="#">Click here to buy now!</a></p></div>';
	}

}

if( $auth->access_level === "Free" ) {

	$memlock = '<i class="fa fa-unlock green"></i>';
	$vidcode = do_shortcode('[KGVID width="640" height="480" view_count="true"]' . $vidurl . '[/KGVID]');
}

$player = get_post_meta($post->ID, player_presenter, true);
$vidtime = get_post_meta($post->ID, video_length, true);
$vidurl = get_post_meta($post->ID, video_file_url, true);

/*

THE following are also available...

$videoid = $auth->credit_video_id;
$member_id = $auth->member_id;

*/

$settings = array(
				'thumb_w' => 248,
				'thumb_h' => 165,
				'thumb_size' => 'medium',
				'thumb_align' => 'aligncenter',
				'post_content' => 'excerpt',
				'comments' => 'both'
				);


$settings = woo_get_dynamic_values( $settings );

$title_before = '<h1 class="title entry-title">';
$title_after = '</h1>';

$page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

woo_post_before();
?>
<article <?php post_class(); ?>>
<?php
woo_post_inside_before();
$postthumb = woo_image( 'width=248&height=165&class=size-medium aligncenter');

if ( 'content' != $settings['post_content'] && ! is_singular() )
	/*woo_image( 'width=' . esc_attr( $settings['thumb_w'] ) . '&height=' . esc_attr( $settings['thumb_h'] ) . '&class=thumbnail ' . esc_attr( $settings['thumb_align'] ) );*/
	echo '<div class="postthumb">' . $postthumb . '<div class="vidtime"><span class="vidlength">' .  $vidtime . '</span><span class="vidaccess">' . $memlock . '</span></div></div>';
?>

<?php $vidtitle = the_title( $title_before, $title_after ); ?>
	<header>
	<?php echo $vidtitle . '<div class="vidmeta">by ' . $player . '</div>';  ?>
	</header>
<?php
woo_post_meta();
?>
	<section class="entry">
<?php
if (is_single()) { echo '<div>' . $vidcode . '</div><div style="clear:both"></div>';}
if ( 'content' == $settings['post_content'] || is_single() ) { the_content( __( 'Continue Reading &rarr;', 'woothemes' ) ); } else { the_excerpt(); }
if ( 'content' == $settings['post_content'] || is_singular() ) wp_link_pages( $page_link_args );
?>
	</section><!-- /.entry -->
	<div class="fix"></div>
<?php
woo_post_inside_after();
?>
</article><!-- /.post -->
<?php
woo_post_after();
$comm = $settings['comments'];
if ( ( 'post' == $comm || 'both' == $comm ) && is_single() ) { comments_template(); }
?>