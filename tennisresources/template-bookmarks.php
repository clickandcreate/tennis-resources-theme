<?php
/**
 * Template Name: Bookmarks Page
 *
 * The page template displays current user's list of bookmarks.
 *
 */

 get_header();
 global $woo_options;
 
 woo_content_before(); ?>

    <div id="content" class="col-full">

    	<div id="main-sidebar-container">

            <?php woo_main_before(); ?>

            <section id="main" class="col-left"><?php

                if( is_user_logged_in() ) { ?>
              <h3 class="accounthead">My Member Account</h3>
              <p>Need to update your account or change your password?<br />
                <a href="https://customer.uspta.org/PROCESSOR/Default.aspx" class="button" target="_blank">Click here to go to your USPTA account</a>
              </p>

                    <h3 class="accounthead">Subscriptions</h3>
<?php

                    $subscriptions = apply_filters( 'trapi_video_subscriptions', get_current_user_id() );
                    asort($subscriptions);
                    
                    foreach ($subscriptions as $subscription_type => $list) {
                    
                    if ($subscription_type == 'Site') {
                      $subscription_type = 'Tennis Resources Site Membership';
                    }
                    else if ($subscription_type == 'ALaCarteItem') {
                      $subscription_type = 'Pay Per Video';
                    }
                    ?>

                        <p><strong><?php echo $subscription_type; ?></strong></p>
              
              <?php

                        trapi_print_video_ul( $list );
                    }


                    $bookmarks = apply_filters( 'trapi_video_bookmarks', get_current_user_id() );

                    if( count($bookmarks) > 0 ) {?>

                        <h3 class="accounthead">Bookmarks</h3>
<?php

                        trapi_print_video_ul( $bookmarks );
                    }
                }?>

            </section><?php 
            
            woo_main_after();
            get_sidebar(); ?>
		</div><!-- /#main-sidebar-container -->
      </script>
    </div><!-- /#content -->
	<?php woo_content_after(); ?>
<script type="text/javascript">
jQuery(window).on('load', function() {
		if(jQuery(this).width() > 767) {

			var tall = 0; 

			jQuery('li.bookmark').each(function() {
				var itemHeight = jQuery(this).outerHeight();
				if(itemHeight > tall) {
					tall = itemHeight
				}
			});

			jQuery('li.bookmark').css('min-height', tall);
		}
	});
</script>
<?php get_footer(); ?>