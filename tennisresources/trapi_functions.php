<?php

function trapi_get_vidcode_video( $education_id, $view, $video_height, $video_poster, $video_file_url ) {
    
    $vidcode = '';

    if( ! empty($education_id) ) {

        if( !isset($_GET['view']) && $_GET['width'] > 1024) {
            $vidcode = '<div class="vidframe"><div class="choosecredit"><div class="innernon">Are you viewing this video for education credit?<br /><br /><input type="radio" name="credit" value="credit" /> YES <input type="radio" name="credit" value="noncredit" />NO';
            if (!is_user_logged_in()) {
                $vidcode .= '<p style="color:#ec8441;">USPTA Members must be logged in to receive education credit</p>';
            }
            $vidcode .= '</div></div></div>';
        }
        if ( !isset($_GET['view']) && $_GET['width'] < 1024) {
           $vidcode = '<div class="vidframe"><div class="choosecredit"><div class="innernon">Are you viewing this video for education credit?<br /><br /><span style="color:rgba(255,255,255,0.4);"><input type="radio" name="credit" value="credit" disabled /> YES</span> <input type="radio" name="credit" value="noncredit" />NO';
           $vidcode .= '<p style="color:#ec8441;">Viewing for credit is not available for mobile devices at this time. Click No to view the video.</p></div></div></div>';
        }
    }

    if( empty($vidcode) ){

        $player_for_credit = $view == 'credit' ? ' controlbar="none" ' : '';

        $vidcode = '<div class="vidframe">' . do_shortcode('[KGVID width="100%" height="' . $video_height . '" view_count="true" poster="' . $video_poster . $player_for_credit .'"]http://no-cache.uspta.com/trc/' . $video_file_url . '[/KGVID]') . '</div>'; 
    }

    return $vidcode;
}

function trapi_get_vidcode_audio( $video_file_url ) {
    
    return '<div class="vidframe">' . do_shortcode('[audio mp3="http://no-cache.uspta.com/trc/' . $video_file_url . '"][/audio]') . '</div>';
}

function trapi_get_vidcode_document( $file_url ) {
    
    return '<p><a class="button" href="' . $file_url . '" target="_blank">Click here to download</a></p>';
}

function trapi_get_vidcode_article( $html_content_url ) {
    
    return '<p><a class="button" href="' . $html_content_url . '" target="_blank">Click here to read this article</a></p><p>&nbsp;</p>';
}

function trapi_get_vidcode( $media_type_id, $education_id, $view, $video_height, $video_poster, $video_file_url, $file_url, $html_content_url ) {
    
    $vidcode = '';
    
    if( $media_type_id == 'Video' )
        $vidcode = trapi_get_vidcode_video( $education_id, $view, $video_height, $video_poster, $video_file_url );

    if( $media_type_id == 'Audio' )
        $vidcode = trapi_get_vidcode_audio( $video_file_url );

    if ($media_type_id == 'Document')
        $vidcode = trapi_get_vidcode_document( $file_url );

    if ($media_type_id == 'Article')
        $vidcode = trapi_get_vidcode_article( $html_content_url );
    
    return $vidcode;
}

function trapi_get_media_type_thumbnail( $media_type ){

    if ($media_type == 'Video')
        return '/wp-content/uploads/2016/01/defaultthumb_video.jpg';

    if ($media_type == 'Audio')
        return '/wp-content/uploads/2016/01/defaultthumb_audio.jpg';

    if ($media_type == 'Document')
        return '/wp-content/uploads/2016/01/defaultthumb_download.jpg';

    if ($media_type == 'Article')
        return '/wp-content/uploads/2016/01/defaultthumb_article.jpg';

    return ''; // default should go here... if any...
}

function trapi_get_media_image_thumbnail( $post_id, $media_type, $size = 'thumbnail' ){

    $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), $size );

    if( ! empty($thumbnail) )
        return $thumbnail['0'];

    return trapi_get_media_type_thumbnail( $media_type );
}
    
function trapi_print_video_li( $post_id ) {

    $post = WP_Post::get_instance( $post_id );
    $player_presenter = get_post_meta($post->ID, 'player_presenter', true);
    $media_type = get_post_meta($post->ID, 'media_type_id', true);
    $thumbnail = trapi_get_media_image_thumbnail( $post->ID, $media_type );?>
    <li class="bookmark"><img src="<?php echo $thumbnail; ?>" class="bookmarkthumb" /><br /><strong><a href="<?php echo $post->guid; ?>"><?php echo $post->post_title; ?></a></strong><br /><br /><?php echo $player_presenter; ?><br /><br /><strong><a href="<?php echo $post->guid; ?>">View</a></strong>
    </li><?php
}

function trapi_print_video_ul( $list ) {

    if( count($list) == 0 )
        return;?>

    <ul><?php
        foreach ($list as $post_id) {
        	trapi_print_video_li( $post_id );
        }?>
    </ul><?php
}

function trapi_print_video_postthumb( $post_id, $thumbnail, $video_length, $memlock ) { ?>
	<div class="postthumb">
		<a href="<?php echo get_permalink( $post_id ); ?>">
			<img class="woo-image size-medium aligncenter" src="<?php echo $thumbnail; ?>" width="248" height="141" />
		</a>
		<div class="vidtime">
			<span class="vidlength"><?php echo $video_length; ?></span><?php
			if( !empty($memlock) ) {?>
				<span class="vidaccess"><?php echo $memlock; ?></span><?php
			}?>
		</div>
	</div><?php	
}
