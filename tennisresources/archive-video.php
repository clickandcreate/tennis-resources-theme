<?php
/**
 * Archive Template
 *
 * The archive template is a placeholder for archives that don't have a template file. 
 * Ideally, all archives would be handled by a more appropriate template according to the
 * current page context (for example, `tag.php` for a `post_tag` archive).
 *
 * @package WooFramework
 * @subpackage Template
 */

 global $woo_options;
 get_header();
?>      
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <div id="content" class="col-full">
    
    	<div id="main-sidebar-container">    
		
            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main" class="col-left">
              <div class="viewselect">
                <div class="viewselector">View By:&nbsp;&nbsp;&nbsp;&nbsp;<a class="gridbtn"><i class="fa fa-th-large"></i>
</a>&nbsp;&nbsp;<a class="listbtn"><i class="fa fa-list"></i></a><input type="radio" value="Grid" name="viewselection" checked="checked" style="display:none;" /> <input type="radio" value="List" name="viewselection" style="display:none;" /></div>
              <div class="filtlinks">Applied Filters:&nbsp;&nbsp;&nbsp;&nbsp;</div><div class="sortlink">
                <!--<select id="mediaselect"><option value="">Sort By</option><option value="video">Video</option><option value="audio">Audio</option><option value="article">Article</option><option value="lesson">Lesson</option><option value="drill">Drill</option><option value="credit">Watch For Credit</option></select>-->
                <select id="mediaselect">
                  <option value="">Sort By</option>
                  <option value="title">A-Z by Title</option>
                  <option value="presenter">A-Z by Presenter</option>
                  <option value="shortlong">Duration (shortest to longest)</option>
                  <option value="longshort">Duration (longest to shortest)</option>
                </select>
              </div>
<div style="clear:both"></div></div>

              <div style="clear:both">&nbsp;</div>
            	
			<?php get_template_part( 'loop', 'archive' ); ?>
                    
            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>
    
		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar( 'alt' ); ?>
      
    </div><!-- /#content -->
	<?php woo_content_after(); ?>
		
<?php get_footer(); ?>