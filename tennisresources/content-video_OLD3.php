<?php
/**
 * Video Content Template
 *
 * This template is the default page content template. It is used to display the content of the
 * `single.php` template file, contextually, as well as in archive lists or search results.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */

if ( ! function_exists( 'trapi_content_video' ) ) {

function trapi_content_video( $post ) {

	$player = get_post_meta($post->ID, player_presenter, true);
	$vidtime = get_post_meta($post->ID, video_length, true);
	$vidurl = get_post_meta($post->ID, video_file_url, true);
	$videoid = get_post_meta($post->ID, credit_video_id, true);
	/*$access = wp_get_post_terms( $post->ID, 'access', array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names' ) );*/
	$buynow = get_post_meta($post->ID, buy_now_url, true);
	$mobilevid = get_post_meta($post->ID, mobile_video_file_url, true);
	$creditprice = get_post_meta($post->ID, credit_video_price, true);

if (is_single()) {
	if(!isset($_GET['r'])) {     
		echo "<script language=\"JavaScript\">     
		<!--      
		document.location=\"$PHP_SELF?r=1&width=\"+screen.width+\"&Height=\"+screen.height;     
		//-->     
		</script>";     
	}     
	else {         
     
     	if(isset($_GET['width']) && $_GET['width'] > 768) {
               $vidurl = get_post_meta($post->ID, video_file_url, true);
     	}  
	else if(isset($_GET['width']) && $_GET['width'] < 768) {
		$vidurl = get_post_meta($post->ID, mobile_video_file_url, true);
	}	   
     	else {     
               $vidurl = get_post_meta($post->ID, video_file_url, true);
     	}     
	}
} 

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
if (!empty($thumb)) {
$custom_img = $thumb['0'];
}
else {
$custom_img = 'http://tennisresources.candc2.us/wp-content/uploads/2015/12/defaultthumb.jpg';
}


$auth = apply_filters( 'trapi_authorize_user_to_video', $post->ID );
if( empty( $auth ) )
	die('Access denied. Reason unknown');

if( $auth->authorized === false && ($auth->access_level === 'Need Video' || 
									$auth->access_level === 'Need Level' || 
									$auth->access_level === 'Need MemberID' || 
									$auth->access_level === 'Invalid Level') ) {

	echo  "<h4 style=\"color:red;\">ERROR: ";

	if( $auth->access_level === 'Invalid Level' )
		echo  "Invalid access level: $auth->invalid_level";
	else
		echo $auth->access_level;

	echo "<br/> for Video [". the_title_attribute( array( 'echo' => 0 ) ) ."]</h4>";
	return;	
}

//echo "<br/>$auth->access_level<br/>";

if( $auth->access_level === "Members Only" ) {

	$memlock = '<i class="fa fa-lock red"></i>';	

	if( ! $auth->authorized ) {

		$vidcode = '<div class="nonmember"><p>Ready to learn?  Let\'s get started!<br /><a href="/login">Log in</a> or <a href="/register">Sign up</a> today!</p></div>';
	}
}

if( $auth->access_level === "Pay Per Video" ) {

	$memlock = '<i class="fa fa-shopping-cart yellow"></i>';

	if( ! $auth->authorized ) {
		
		$vidcode = '<div class="nonmember"><p>This video is available to purchase<br /><a href="#">Click here to buy now!</a></p></div>';
	}

}

if( $auth->access_level === "Free" ) {

	$memlock = '<i class="fa fa-unlock green"></i>';
	$vidcode = do_shortcode('[KGVID width="640" height="480" view_count="true" poster="' . $custom_img . '"]http://no-cache.uspta.com/trc/' . $vidurl . '[/KGVID]');
}


/*

THE following are also available...

$videoid = $auth->credit_video_id;
$member_id = $auth->member_id;

*/

$settings = array(
				'thumb_w' => 248,
				'thumb_h' => 165,
				'thumb_size' => 'medium',
				'thumb_align' => 'aligncenter',
				'post_content' => 'excerpt',
				'comments' => 'both'
				);
$settings = woo_get_dynamic_values( $settings );
$title_before = '<h1 class="title entry-title">';
$title_after = '</h1>';

if ( ! is_single() ) {
$title_before = '<h2 class="looptitle">';
$title_after = '</h2>';
$title_before = $title_before . '<a href="' . esc_url( get_permalink( get_the_ID() ) ) . '" rel="bookmark" title="' . the_title_attribute( array( 'echo' => 0 ) ) . '">';
$title_after = '</a>' . $title_after;
}

$page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

woo_post_before();
?>
<article <?php post_class(); ?>>
<?php
woo_post_inside_before();

if ( 'content' != $settings['post_content'] && ! is_singular() )
	/*woo_image( 'width=' . esc_attr( $settings['thumb_w'] ) . '&height=' . esc_attr( $settings['thumb_h'] ) . '&class=thumbnail ' . esc_attr( $settings['thumb_align'] ) );*/
	echo '<div class="postthumb"><a href="' . get_permalink( get_the_ID() ) . '"><img class="woo-image size-medium aligncenter" src="' . $custom_img . '" width="248" height="165" /></a><div class="vidtime"><span class="vidlength">' .  $vidtime . '</span><span class="vidaccess">' . $memlock . '</span></div><div>' . $vidpermission . '</div></div>';
?>

<?php $vidtitle = the_title( $title_before, $title_after ); ?>
	<header>
	<?php echo $vidtitle . '<div class="vidmeta">by ' . $player . '</div>';  
		if (!empty($buynow)) {
			echo '<div class="buynow"><a target="_blank" href="' . $buynow . '" class="button">Purchase DVD</a></div>';
		}
	?>
	</header>
<?php
woo_post_meta();
?>
	<section class="entry">
<?php
if (is_single()) { echo '<div>' . $vidcode . '</div><div style="clear:both"></div>';}
if ( 'content' == $settings['post_content'] || is_single() ) { the_content( __( 'Continue Reading &rarr;', 'woothemes' ) ); } else { the_excerpt(); }
if ( 'content' == $settings['post_content'] || is_singular() ) wp_link_pages( $page_link_args );
?>
	</section><!-- /.entry -->
	<div class="fix"></div>
<?php
woo_post_inside_after();
?>
</article><!-- /.post -->
<?php
woo_post_after();
$comm = $settings['comments'];
if ( ( 'post' == $comm || 'both' == $comm ) && is_single() ) { comments_template(); }
}
}

trapi_content_video( $post );
?>