<?php
/** * Archive Template * * The archive template is a placeholder for archives that don't have a template file.  * Ideally, all archives would be handled by a more appropriate template according to the * current page context (for example, `tag.php` for a `post_tag` archive). * * @package WooFramework * @subpackage Template */ global $woo_options; get_header();?>      
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <div id="content" class="col-full">
    
    	<div id="main-sidebar-container">    
		
            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main" class="col-left">

              <div class="viewselect">
                Select View:&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" value="Grid" name="viewselection" checked="checked" /> Grid &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" value="List" name="viewselection" /> List
              </div>
              <hr style="clear:both; height:1px;" />
<div class="filtlinks"></div>
              <div style="clear:both">&nbsp;</div>
            	
			<?php get_template_part( 'loop', 'archive' ); ?>
                    
            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>
    
		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar( 'alt' ); ?>
      <script type="text/javascript">        
jQuery(document).ready(function(){
	if (jQuery('body').attr('class').contains('video')) {
		if (jQuery("input[name='viewselection']").is(':checked') && jQuery("input[name='viewselection']:checked").val() == 'Grid') {
			jQuery("section#main").addClass('gridvid');
			jQuery("section#main").removeClass('listvid');
			jQuery(".woo-image").addClass('aligncenter');
			jQuery(".woo-image").removeClass('alignleft');
		}
		else if (jQuery("input[name='viewselection']").is(':checked') && jQuery("input[name='viewselection']:checked").val() == 'List') {
			jQuery("section#main").addClass('listvid');
			jQuery("section#main").removeClass('gridvid');
			jQuery(".woo-image").removeClass('aligncenter');
			jQuery(".woo-image").addClass('alignleft');
		}
		else {
			jQuery("section#main").addClass('gridvid');
		}
	}
	else {
		jQuery("div.viewclass").hide();        
	}        
});

jQuery("input[name='viewselection']").click(function(){        
	if (jQuery("input[name='viewselection']:checked").val() == 'Grid') {
		jQuery("section#main").addClass('gridvid');
		jQuery("section#main").removeClass('listvid');
		jQuery(".woo-image").addClass('aligncenter');
		jQuery(".woo-image").removeClass('alignleft');        
	}        
	else if (jQuery("input[name='viewselection']:checked").val() == 'List') {
		jQuery("section#main").addClass('listvid');
		jQuery("section#main").removeClass('gridvid');
		jQuery(".woo-image").removeClass('aligncenter');
		jQuery(".woo-image").addClass('alignleft');
	}           
});      
</script>
    </div><!-- /#content -->
	<?php woo_content_after(); ?>
		
<?php get_footer(); ?>