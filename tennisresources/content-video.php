<?php
/**
 * Video Content Template
 *
 * This template is the default page content template. It is used to display the content of the
 * `single.php` template file, contextually, as well as in archive lists or search results.
 *
 * @package WooFramework
 * @subpackage Template
 */
/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */
if ( ! function_exists( 'trapi_content_video' ) ) {

	function trapi_content_video( $post ) {
		$player_presenter = get_post_meta($post->ID, 'player_presenter', true);
		$video_length = get_post_meta($post->ID, 'video_length', true);
		$video_file_url = get_post_meta($post->ID, 'video_file_url', true);
		$html_content_url = get_post_meta($post->ID, 'html_content_url', true);
		$file_url = get_post_meta($post->ID, 'file_url', true);
		$credit_video_id = get_post_meta($post->ID, 'credit_video_id', true);
		//$buy_now_url = get_post_meta($post->ID, 'buy_now_url', true);
		$mobile_video_file_url = get_post_meta($post->ID, 'mobile_video_file_url', true);
		$credit_video_price = get_post_meta($post->ID, 'credit_video_price', true);
		$member_price = get_post_meta($post->ID, 'member_price', true);
		$media_type_id = get_post_meta($post->ID, 'media_type_id', true);
		$education_id = get_post_meta($post->ID, 'education_id', true);
		$number_credits = get_post_meta($post->ID, 'number_credits', true);
		$ppv_link = get_post_meta($post->ID, 'ppv_purchase_link', true);

		if (empty($ppv_link)) {
			$ppv_link = 'https://customer.uspta.org/processor/USPTAMembers/Cart/USPTAMembers/Store/TR.aspx';
		}


		$purchasepop = '<div id="purchase-pop" class="modal" style="text-align:center; display:none;">';
		$purchasepop .= '<p>You will now be taken to the USPTA web site to complete your purchase.  If you do not have a "My USPTA" account, you will be asked to create one.  This is the same account you will use to login here at TennisResources.com.<br /></p>';
		$purchasepop .= '<p>After you have completed your purchase, come back to the TennisResources.com web site and log in to view the videos.  If you are purchasing a Pay-Per-Video, the video will appear in your My Account page here on TennisResources.com within 5 minutes after your purchase.<br /></p>';
		$purchasepop .= '<p><a class="button" href="' . $ppv_link . '" target="_blank">Click here to continue</a></p></div>';

		echo $purchasepop;

		$endcreditmsg = '<div id="endcreditmsg" class="modal" style="text-align:center; display:none;">';
		$endcreditmsg .= '<p>Congratulations! You have completed the video, and ' . $number_credits . ' educational credits have been submitted to USPTA.</p>';
		$endcreditmsg .= '<p><a href="#close" rel="modal:close">OK</a></p></div>';

		echo $endcreditmsg;


		$view = isset($_GET['view']) ? $_GET['view'] : ''; 

		if( $video_length == ':' ) {
			$video_length = '';
			}
			else {
			$vidtime = explode(':', $video_length);
			$timecount = count($vidtime);
			if ($vidtime[0] > 60) {
				$vidhour = floor($vidtime[0]/60);
				$vidhour = str_pad($vidhour, 2, '0', STR_PAD_LEFT);
				$vidminute = $vidtime[0] % 60;
				if ($vidminute < 10) {
					$vidminute = str_pad($vidminute, 2, '0', STR_PAD_LEFT);
				}
				$vidsecond = $vidtime[1];
				if ($vidsecond < 10) {
					$vidsecond = str_pad($vidsecond, 2, '0', STR_PAD_LEFT);
				}
				$video_length = $vidhour .':'.$vidminute.':'.$vidsecond;
			}
			else {
				if ($timecount == '2') {
					$vidminute = $vidtime[0];
					if ($vidminute < 10) {
						$vidminute = str_pad($vidminute, 2, '0', STR_PAD_LEFT);
					}
					$vidsecond = $vidtime[1];
					if ($vidsecond < 10) {
						$vidsecond = str_pad($vidsecond, 2, '0', STR_PAD_LEFT);
					}
					$video_length = $vidminute.':'.$vidsecond;
				}
				else if ($timecount == '3') {
					$vidhour = $vidtime[0];
					if ($vidhour < 10) {
						$vidhour = str_pad($vidminute, 2, '0', STR_PAD_LEFT);
					}
					$vidminute = $vidtime[1];
					if ($vidminute < 10) {
						$vidminute = str_pad($vidsecond, 2, '0', STR_PAD_LEFT);
					}
					$vidsecond = $vidtime[2];
					if ($vidsecond < 10) {
						$vidsecond = str_pad($vidsecond, 2, '0', STR_PAD_LEFT);
					}
					$video_length = $vidhour .':'.$vidminute.':'.$vidsecond;
				}
			}
			}

		if (is_single()) {
			if(!isset($_GET['r'])) {     
				echo "<script language=\"JavaScript\">     
				<!--      
				document.location=\"$PHP_SELF?r=1&width=\"+screen.width+\"&Height=\"+screen.height;     
				//-->     
				</script>";
			}     
			else {
		     	$screen_width = isset($_GET['width']) ? intval($_GET['width']) : 0;
		     	$video_height = '480';
				if( $screen_width < 768 && !empty($mobile_video_file_url) ) {
					$video_file_url = $mobile_video_file_url;
				}
				else if( $screen_width < 640 && $screen_width > 400 ) {
					$video_height = '250';
				}
				else if( $screen_width < 400 ) {
					$video_height = '200';
				}
			}
		} 
		$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
		$custom_img = '/wp-content/uploads/2015/12/defaultthumb.jpg';
		$video_poster = '/wp-content/uploads/2016/01/defaultthumb_videonoicon.jpg';
		$custom_img = !empty($thumbnail) ? $thumbnail['0'] :  trapi_get_media_type_thumbnail( $media_type_id );
		$auth = apply_filters( 'trapi_authorize_user_to_video', $post->ID );
		if( empty( $auth ) ) {
			die('Access denied. Reason unknown');
		}
		if( $auth->authorized === false && ($auth->access_level === 'Need Video' || 
											$auth->access_level === 'Need Level' || 
											$auth->access_level === 'Need MemberID' || 
											$auth->access_level === 'Invalid Level') ) {
			echo  "<h4 style=\"color:red;\">ERROR: ";

			if( $auth->access_level === 'Invalid Level' ) {					
			echo  "Invalid access level: $auth->invalid_level. ";
			} else {				
			echo "Unknown access level: $auth->access_level. ";
			}

			echo "<br/> for Video [". the_title_attribute( array( 'echo' => 0 ) ) ."] <strong style=\"color:red;\"> Contact site administrator!</strong></h4>";				return;	
								}
		$vidcode = '';
		$memlock = '<i class="fa fa-lock green"></i>';

		if( ! $auth->authorized ) {
			if( $auth->access_level == 'Members Only' ) {
				$memlock = '<i class="fa fa-lock red"></i>';
				/*if (!empty($education_id)) {
					$memlock .= '<i class="fa fa-graduation-cap red"></i>';
				}*/		$className = strtolower( $media_type_id ); // video | audio | article | document
			$vidcode = '<div class="vidframe"><div class="nonmember' . $className . '"><div class="innernon">Ready to learn?  Let\'s get started!<br /><a href="/login">Log in</a> or <a href="#join-pop" rel="modal:open">Sign up</a> today!</div></div></div>';
			}
			if( $auth->access_level === 'Pay Per Video' ) {
				$memlock = '<i class="fa fa-shopping-cart yellow"></i>';
				if ($media_type_id == 'Video' ||  $media_type_id == 'Audio')
					$vidcode = '<div class="vidframe"><div class="nonmembervideo"><div class="innernon">This video is available to purchase<br /><a href="#purchase-pop" rel="modal:open">Click here to buy now!</a></div></div></div>';
			}
		}
		else { // authorized!
		if( $auth->access_level == 'Members Only' ) {
				$vidcode = trapi_get_vidcode( $media_type_id, $education_id, $view, $video_height, $video_poster, $video_file_url, $file_url, $html_content_url );

			}		if( $auth->access_level === 'Pay Per Video' ) {
					$memlock = '<i class="fa fa-shopping-cart green"></i>';
		
					if( $media_type_id == 'Video' )
					$vidcode = trapi_get_vidcode_video( $education_id, $view, $video_height, $video_poster, $video_file_url );
					
					if( $media_type_id == 'Audio' )
					$vidcode = trapi_get_vidcode_audio( $video_file_url );
		}
		}
		if( $auth->access_level === "Free" ) {
			$memlock = '<i class="fa fa-unlock green"></i>';
			$vidcode = trapi_get_vidcode( $media_type_id, $education_id, $view, $video_height, $video_poster, $video_file_url, $file_url, $html_content_url );
		}
		if( empty($vidcode) ) {
			$vidcode = '<div class="vidframe">' . do_shortcode('[KGVID width="100%" height="' . $video_height . '" view_count="true" poster="' . $video_poster . '"]http://no-cache.uspta.com/trc/' . $video_file_url . '[/KGVID]') . '</div>';
		}
		$settings = array(
						'thumb_w' => 248,
						'thumb_h' => 141,
						'thumb_size' => 'medium',
						'thumb_align' => 'aligncenter',
						'post_content' => 'excerpt',
						'comments' => 'both'						);
		$settings = woo_get_dynamic_values( $settings );
		$title_before = '<h1 class="title entry-title">';
		$title_after = '</h1>';				if ( ! is_single() ) {
			$thetitle = $post->post_title;			$shorttitle = strlen($thetitle) > 40 ? (substr($thetitle,0,40) . '...') : $thetitle;			$title_before = '<h2 class="looptitle">';			$title_after = '</h2><div style="display:none;">access_level: ' . $auth->access_level . '<br/>' . ($auth->authorized == false ? 'NOT ' : '') . 'authorized<br/>mediatype: ' . $media_type_id . '</div>';			$title_before = $title_before . '<a href="' . esc_url( get_permalink( get_the_ID() ) ) . '" rel="bookmark" title="' . the_title_attribute( array( 'echo' => 0 ) ) . '">';			$title_after = '</a>' . $title_after;			$shortplayer =  strlen($player_presenter) > 25 ? (substr($player_presenter,0,25) . '...') : $player_presenter;
		}
		$page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );
		woo_post_before();?>		<article <?php post_class(); ?>><?php						woo_post_inside_before();			if ( 'content' != $settings['post_content'] && ! is_singular() ){				trapi_print_video_postthumb( get_the_ID(), $custom_img, $video_length, $memlock );
			} 
			$vidtitle = $title_before . $shorttitle . $title_after;?>
			<header><?php 
				if( isset( $_GET['showauth'] ) ) {
					echo '<div><strong>' . print_r( $auth, true ) . '</strong></div>';
				}

				$mediacredit = '<div class="vidmeta"><span class="mediatype-' . $media_type_id . '">' . $media_type_id . '</span>';
				if (!empty($education_id)) {
					$mediacredit .= '<span class="creditlabel">For Credit</span>';
				}
				$mediacredit .= '</div>';
				echo $vidtitle . '<div class="vidmeta">by ' . $shortplayer . '</div>';
				if (!is_single()) {					
				echo $mediacredit;
				}?>

				</header>
				<?php
			woo_post_meta();?>
			<section class="entry"><?php
				if (is_single()) { 					echo '<div>' . $vidcode . '</div><div style="clear:both"></div>';
					if( isset($_GET['view']) && $_GET['view'] == 'credit' && is_user_logged_in() ) {?>
						<div id="trapi-player-control" class="trapi-player-pause"><span id="trapi-player-pause">Pause</span> <span id="trapi-player-play">Play</span></div><?php
					}
					/*if (!empty($buy_now_url)) {
						echo '<div class="buydvd"><a target="_blank" href="' . $buy_now_url . '" class="button">Purchase DVD</a></div>';
					}*/
					if (!empty($player_presenter)) {
						echo '<div class="buynow"><strong>Presenter: ' . $player_presenter . '</strong></div>';
					}

					if (!empty($number_credits)) {
						if (!empty($member_price) || !empty($credit_video_price)) {
							echo '<div  class="creditbuy"><p>';
							if ($auth->access_level == "Members Only") {
								echo '<span style="float:right;"><a class="button" href="#purchase-pop" rel="modal:open">Click here to purchase</a></span>';
							}
							echo '<strong>Price</strong><br />USPTA Members: <strong>$' . $member_price . '</strong>, Non-Members: <strong>$' . $credit_video_price .'</strong><br />USPTA Educational Credits: ' . $number_credits . '</p></div>';
						}
						else {
							echo '<div class="creditbuy"><p><strong>Price</strong><br />USPTA Members: <strong>FREE</strong>, Non-Members: <strong>FREE</strong><br />USPTA Educational Credits: ' . $number_credits . '</p></div>';
						}
					}
					do_shortcode( '[trapi_player]' );
				}
				if ( 'content' == $settings['post_content'] || is_single() ) { 

					the_content( __( 'Continue Reading &rarr;', 'woothemes' ) ); 
					if( is_user_logged_in() && $post->post_type == 'video' ){
						$bookmark_href = 'remove';
						$bookmark_href_text = 'Remove Bookmark';
						$bookmark_id = apply_filters( 'trapi_video_bookmark', $post->ID );
						if( empty($bookmark_id) ) {
							$bookmark_id = $post->ID;
							$bookmark_href = 'add';
							$bookmark_href_text = 'Bookmark';
						}?>
						<a id="bookmark_action" href="<?php echo $bookmark_href; ?>" post_id="<?php echo $bookmark_id; ?>"><?php echo $bookmark_href_text; ?></a><?php
					}
				} 
				else { 
					the_excerpt();
				}
				if ( 'content' == $settings['post_content'] || is_singular() ) {
					wp_link_pages( $page_link_args );
				}?>
			</section><!-- /.entry -->
			<div class="fix"></div><?php
			woo_post_inside_after();?>
		</article><!-- /.post --><?php
		woo_post_after();
		$comm = $settings['comments'];
		if ( ( 'post' == $comm || 'both' == $comm ) && is_single() ) { 
			comments_template(); 
		}
	}
}
trapi_content_video( $post );
?>
<script type="text/javascript">
jQuery(document).ready(function(){
 	var vidpage = window.location.toString();
	if (vidpage.indexOf('view=credit') != -1) {
		jQuery('.vjs-progress-holder').css('display','none');
	}
	jQuery('#bookmark_action').click(function( event ){
		event.preventDefault();
		var token = jQuery(this).attr('href');
		var post_id = jQuery(this).attr('post_id');
		jQuery.ajax({ 
			type: 'POST',
			url: "<?php echo admin_url('admin-ajax.php'); ?>",			dataType: 'json',
			data: {
				action: 'bookmark_' + token,
				post_id: post_id
			},
			success: function( data, textStatus, jqXHR ) {
				console.log( data );
				window.location.reload();
			}
		});
	});
});
</script>
<?php if (!is_user_logged_in()) { ?>
<script type="text/javascript">
	jQuery("input[name='credit']").click(function(){
		var currentpage = window.location;
		var encodedpage = encodeURIComponent(currentpage);
		var creditval = jQuery("input[name='credit']:checked").val();
		if (creditval == 'noncredit') {
			document.location = currentpage+"&view="+creditval;
		}
		else {
			document.location = "/login/?redirect_to="+encodedpage+"%26view%3D"+creditval;
		}
	});
</script>
<?php } else { ?>
<script type="text/javascript">
	jQuery("input[name='credit']").click(function(){
		var currentpage = window.location;
		var creditval = jQuery("input[name='credit']:checked").val();		
		document.location = currentpage+"&view="+creditval;
	});
	window.trapi_member_credited = function(data) {
		console.log('post_id ' + data.post_id);
		console.log('user_id ' + data.user_id);

		jQuery("#endcreditmsg").modal();
	}
</script>

<?php } ?>